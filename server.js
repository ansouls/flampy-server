const express = require('express')
const app = express()

const {isUUID} = require('validator')

const config = require('./config')
const lamp = require('./status_lamp')
const server = require('./status_server')

const db = require('better-sqlite3')(config.DB_NAME)

let DEBUG = 0
let pairs = {}
let queue = {}

function init() {
  process.argv.forEach((v, i) => {
    if (v.toLowerCase().indexOf('debug') !== -1) {
      DEBUG = 1
    }
  })

  console.log(`|\x1b[33m DEBUG ${DEBUG ? '\x1b[1;32mON' : '\x1b[1;31mOFF'}\x1b[0m`)

  try {
    db.prepare(`select * from ${config.TABLE_NAME}`).all().forEach(el => {
      pairs[el[0]] = el[1]
      if(DEBUG) console.log(`| \x1b[33m+pair \x1b[1;34m${el[0]}:${el[1]}\x1b[0m`)
    })
  } catch (e) {
    if (e.message.toLowerCase().indexOf('no such table') !== -1)
      db.prepare(`CREATE TABLE \`${config.TABLE_NAME}\` ( \`0\` TEXT NOT NULL UNIQUE, \`1\` TEXT UNIQUE, PRIMARY KEY(\`0\`,\`1\`) );`).run()
  }
}

function log(req, res, next) {
  if (DEBUG) console.log(`| [${timestamp()}] \x1b[33mget ${req.originalUrl} by \x1b[1;34m${req.ip}\x1b[0m`)
  next()
}

function timestamp() {
  return new Date().toGMTString().split(' ').slice(-2, -1)[0]
}

function generateUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function checkUUID(req, res) {
  if (!isUUID(req.query.uuid)) {
    res.status(403).end()
    return false
  } else return true
}

function getLinked(id) {
  res = null
  Object.keys(pairs).forEach(el => {
    if (el == id) res = pairs[el]
    else if (pairs[el] == id) res = el
  })
  return res
}

function registerAloneDevice() {
  return new Promise((resolve, reject) => {
    try {
      uuid = generateUUID()
      id = uuid.split('-')[0]
      db.prepare(`insert into ${config.TABLE_NAME} values(?, null)`).run(id)
      pairs[id] = null
      resolve(uuid)
    } catch (e) {
      if (e.message.toLowerCase().indexOf('unique constraint failed') !== -1)
        reject(lamp.REG_FAIL_EXIST)
      else
        reject(lamp.REG_FAIL_UNK, e.message)
    }
  })
}

function nullPairs(id) {
  return new Promise((resolve, reject) => {
    try {
	resolve(db.prepare(`select "0" from ${config.TABLE_NAME} where ("1" is null and "0" is not @id)`).all({id: id}))
    } catch (e) {
      reject(e.message)
    }
  })
}

function linkCreate(id, pair) {
  return new Promise((resolve, reject) => {
    try {
      db.prepare(`update ${config.TABLE_NAME} set "1" = @id where "0" = @pair`).run({id: id, pair: pair})
      db.prepare(`update ${config.TABLE_NAME} set "1" = @pair where "0" = @id`).run({id: id, pair: pair})
      pairs[id] = pair
      pairs[pair] = id
      resolve()
    } catch (e) {
      reject(server.UNK_ERROR, e.message)
    }
  })
}

function linkDelete(id, pair) {
  return new Promise((resolve, reject) => {
    try {
      db.prepare(`update ${config.TABLE_NAME} set "1" = null where "0" = @pair`).run({pair: pair})
      db.prepare(`update ${config.TABLE_NAME} set "1" = null where "0" = @id`).run({id: id})
      pairs[id] = null
      pairs[pair] = null
      resolve()
    } catch (e) {
      reject(server.UNK_ERROR, e.message)
    }
  })
}

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.post('/register', log, (req, res) => {
  registerAloneDevice()
    .then(uuid => {
      console.log(`| [${timestamp()}] \x1b[33mgenerated UUID: \x1b[1;34m${uuid}\x1b[0m`)
      res.status(server.REG_SUC).send(uuid)
    })
    .catch((s, msg) => {
      console.log(s, msg)
      switch (s) {
        case 0x11:
          res.status(server.REG_FAIL_EXIST).end()
          break
        case 0x1F:
          res.status(server.REG_FAIL_UNK).end(msg)
          break
        default:
          res.status(server.UNK_ERROR).send(msg)
          break
      }
    })
})

app.post('/link.create', log, (req, res) => {
  if (req.query.id && req.query.pair) {
    linkCreate(req.query.id, req.query.pair)
      .then(i => {
        console.log(`| [${timestamp()}] \x1b[33mpair create: \x1b[1;34m${req.query.id}\x1b[0;33m:\x1b[1;34m${req.query.pair}\x1b[0m`)
        res.status(server.REG_SUC).end()
      })
      .catch((s, msg) => {
        res.status(server.REG_FAIL_UNK).end(msg)
      })
  } else {
    res.status(server.BAD_REQUEST).end()
  }
})

app.post('/link.delete', log, (req, res) => {
  if (req.query.id && req.query.pair) {
    linkDelete(req.query.id, req.query.pair)
      .then(i => {
        console.log(`| [${timestamp()}] \x1b[33mpair delete: \x1b[1;34m${req.query.id}\x1b[0;33mx\x1b[1;34m${req.query.pair}\x1b[0m`)
        res.status(server.REG_SUC).end()
      })
      .catch((s, msg) => {
        res.status(server.REG_FAIL_UNK).end(msg)
      })
  } else {
    res.status(server.BAD_REQUEST).end()
  }
})

app.get('/null', log, (req, res) => {
  if (req.query.id) {
    nullPairs(req.query.id)
      .then(p => {
        console.log(`| [${timestamp()}] \x1b[33msend null pairs to \x1b[1;34m${req.ip} \x1b[0;32m[length: ${p.length}]\x1b[0m`)
        res.status(200).json(p)
      })
      .catch(msg => {
        res.status(server.UNK_ERROR, msg).end()
      })
  } else {
    res.status(server.BAD_REQUEST).end()
  }
})

app.get('/queue', log, (req, res) => {
  if (checkUUID(req, res)) {
    id = req.query.uuid.split('-')[0]

    if (id in queue && queue[id].length > 0) {
      res.status(server.QUEUE_SEND).send(queue[id].shift())
      console.log(`| [${timestamp()}] \x1b[33mqueue shift (\x1b[1;34m${id}\x1b[0;33m) \x1b[32m[left: ${queue[id].length}]\x1b[0m`)
    } else {
      res.status(server.QUEUE_EMPTY).end()
    }
  }
})

app.post('/queue.add', log, (req, res) => {
  if (checkUUID(req, res)) {
    id = getLinked(req.query.uuid.split('-')[0])
    if (!id) res.status(server.QUEUE_NO_PAIR).end()
    else {
      if (!(id in queue)) queue[id] = []
      queue[id].push(req.query.action)
      console.log(`| [${timestamp()}] \x1b[33mqueue add (\x1b[1;34m${req.query.uuid.split('-')[0]}\x1b[0;33m->\x1b[1;34m${id}\x1b[0;33m): \x1b[1;32m${req.query.action}\x1b[0m`)
      res.status(server.QUEUE_SEND).end()
    }
  }
})


app.get('/favicon.ico', (req, res) => { res.sendStatus(404) })
app.get('/cosmos.png', (req, res) => { res.sendFile(__dirname + '/www/cosmos.png') })
app.get('/dummy.css', (req, res) => { res.sendFile(__dirname + '/www/dummy.css') })

app.get('*', log, (req, res) => {
  res.sendFile(__dirname + '/www/dummy.html')
})



init()

app.listen(8080, '0.0.0.0', (err) => { if (err) throw err; else console.log('|\x1b[33m Listening on \x1b[1;35m80\x1b[0;33m port\x1b[0m') })
